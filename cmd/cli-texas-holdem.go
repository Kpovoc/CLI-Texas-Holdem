package main

import (
	"cli-texas-holdem/pkg"
	"fmt"
	"slices"
)

func dealTwoHands() {
	deck := pkg.NewDeck()

	deck.Shuffle()

	var playerA, playerB, board []pkg.Card

	for i := 0; i < 2; i++ {
		playerA = append(playerA, deck.DrawCard())
		playerB = append(playerB, deck.DrawCard())
	}

	for i := 0; i < 5; i++ {
		board = append(board, deck.DrawCard())
	}

	pkg.RenderCards(board)

	cardsA := slices.Concat(board, playerA)
	cardsB := slices.Concat(board, playerB)

	handA := pkg.DetermineHand(cardsA, "Player A")
	handB := pkg.DetermineHand(cardsB, "Player B")

	fmt.Printf("Hand A: %v\n", handA.Name)
	pkg.RenderCards(playerA)

	fmt.Printf("Hand B: %v\n", handB.Name)
	pkg.RenderCards(playerB)

	if handA.Value > handB.Value {
		fmt.Printf("Hand A wins! %v!\n", handA.Name)
	} else if handA.Value < handB.Value {
		fmt.Printf("Hand B wins! %v!\n", handB.Name)
	} else {
		fmt.Printf("It's a draw! \n%v\n%v\n", handA, handB)
	}
}

func renderCardsAndHand(cards []pkg.Card) {
	pkg.RenderCards(cards)
	hand := pkg.DetermineHand(cards, "Player 0")
	fmt.Println("Hand:", hand)
}

func listHands() {
	royalFlush := []pkg.Card{
		{Face: pkg.TEN, Suit: pkg.SPADES},
		{Face: pkg.NINE, Suit: pkg.SPADES},
		{Face: pkg.JACK, Suit: pkg.SPADES},
		{Face: pkg.ACE, Suit: pkg.SPADES},
		{Face: pkg.QUEEN, Suit: pkg.SPADES},
		{Face: pkg.KING, Suit: pkg.SPADES},
		{Face: pkg.EIGHT, Suit: pkg.CLUBS},
	}
	renderCardsAndHand(royalFlush)

	straightFlush := []pkg.Card{
		{Face: pkg.TEN, Suit: pkg.SPADES},
		{Face: pkg.NINE, Suit: pkg.SPADES},
		{Face: pkg.SIX, Suit: pkg.SPADES},
		{Face: pkg.SEVEN, Suit: pkg.SPADES},
		{Face: pkg.QUEEN, Suit: pkg.SPADES},
		{Face: pkg.KING, Suit: pkg.HEARTS},
		{Face: pkg.EIGHT, Suit: pkg.SPADES},
	}
	renderCardsAndHand(straightFlush)

	fourOfAKind := []pkg.Card{
		{Face: pkg.TEN, Suit: pkg.HEARTS},
		{Face: pkg.TEN, Suit: pkg.DIAMONDS},
		{Face: pkg.SIX, Suit: pkg.SPADES},
		{Face: pkg.TEN, Suit: pkg.CLUBS},
		{Face: pkg.QUEEN, Suit: pkg.SPADES},
		{Face: pkg.KING, Suit: pkg.HEARTS},
		{Face: pkg.TEN, Suit: pkg.SPADES},
	}
	renderCardsAndHand(fourOfAKind)

	fullHouse := []pkg.Card{
		{Face: pkg.TEN, Suit: pkg.HEARTS},
		{Face: pkg.TEN, Suit: pkg.DIAMONDS},
		{Face: pkg.SIX, Suit: pkg.SPADES},
		{Face: pkg.NINE, Suit: pkg.CLUBS},
		{Face: pkg.SIX, Suit: pkg.HEARTS},
		{Face: pkg.KING, Suit: pkg.HEARTS},
		{Face: pkg.TEN, Suit: pkg.SPADES},
	}
	renderCardsAndHand(fullHouse)

	flush := []pkg.Card{
		{Face: pkg.TEN, Suit: pkg.SPADES},
		{Face: pkg.NINE, Suit: pkg.SPADES},
		{Face: pkg.FIVE, Suit: pkg.SPADES},
		{Face: pkg.SEVEN, Suit: pkg.SPADES},
		{Face: pkg.QUEEN, Suit: pkg.SPADES},
		{Face: pkg.KING, Suit: pkg.HEARTS},
		{Face: pkg.EIGHT, Suit: pkg.SPADES},
	}
	renderCardsAndHand(flush)

	straight := []pkg.Card{
		{Face: pkg.TEN, Suit: pkg.SPADES},
		{Face: pkg.NINE, Suit: pkg.SPADES},
		{Face: pkg.SIX, Suit: pkg.CLUBS},
		{Face: pkg.SEVEN, Suit: pkg.SPADES},
		{Face: pkg.SEVEN, Suit: pkg.DIAMONDS},
		{Face: pkg.KING, Suit: pkg.HEARTS},
		{Face: pkg.EIGHT, Suit: pkg.SPADES},
	}
	renderCardsAndHand(straight)

	threeOfAKind := []pkg.Card{
		{Face: pkg.TEN, Suit: pkg.HEARTS},
		{Face: pkg.TEN, Suit: pkg.DIAMONDS},
		{Face: pkg.SIX, Suit: pkg.SPADES},
		{Face: pkg.NINE, Suit: pkg.CLUBS},
		{Face: pkg.QUEEN, Suit: pkg.SPADES},
		{Face: pkg.KING, Suit: pkg.HEARTS},
		{Face: pkg.TEN, Suit: pkg.SPADES},
	}
	renderCardsAndHand(threeOfAKind)

	twoPairs := []pkg.Card{
		{Face: pkg.TEN, Suit: pkg.HEARTS},
		{Face: pkg.TEN, Suit: pkg.DIAMONDS},
		{Face: pkg.SIX, Suit: pkg.SPADES},
		{Face: pkg.SIX, Suit: pkg.CLUBS},
		{Face: pkg.QUEEN, Suit: pkg.SPADES},
		{Face: pkg.THREE, Suit: pkg.HEARTS},
		{Face: pkg.THREE, Suit: pkg.SPADES},
	}
	renderCardsAndHand(twoPairs)

	onePair := []pkg.Card{
		{Face: pkg.TEN, Suit: pkg.HEARTS},
		{Face: pkg.TEN, Suit: pkg.DIAMONDS},
		{Face: pkg.SIX, Suit: pkg.SPADES},
		{Face: pkg.FIVE, Suit: pkg.CLUBS},
		{Face: pkg.QUEEN, Suit: pkg.SPADES},
		{Face: pkg.KING, Suit: pkg.HEARTS},
		{Face: pkg.THREE, Suit: pkg.SPADES},
	}
	renderCardsAndHand(onePair)

	highCards := []pkg.Card{
		{Face: pkg.TEN, Suit: pkg.SPADES},
		{Face: pkg.NINE, Suit: pkg.SPADES},
		{Face: pkg.THREE, Suit: pkg.CLUBS},
		{Face: pkg.SEVEN, Suit: pkg.SPADES},
		{Face: pkg.QUEEN, Suit: pkg.DIAMONDS},
		{Face: pkg.KING, Suit: pkg.HEARTS},
		{Face: pkg.EIGHT, Suit: pkg.SPADES},
	}
	renderCardsAndHand(highCards)
}

func gameTest() {
	game := pkg.Game{}
	game.Init()
	pkg.DrawBoard(game)
	_, _ = fmt.Scanln()
	game.AdvanceGame()
	pkg.DrawBoard(game)
	_, _ = fmt.Scanln()
	game.AdvanceGame()
	pkg.DrawBoard(game)
	_, _ = fmt.Scanln()
	game.AdvanceGame()
	pkg.DrawBoard(game)
	_, _ = fmt.Scanln()
	game.AdvanceGame()
	pkg.DrawBoard(game)
	_, _ = fmt.Scanln()
	game.AdvanceGame()
	pkg.DrawBoard(game)
}

func main() {
	//dealTwoHands()
	//listHands()
	gameTest()
}
