# CLI-Texas-Holdem
CLI-Texas-Holdem is a simple command line application that allows you to play Texas Holdem with 3 computer opponents. It draws the board on the command line with UTF-8 encoded characters.  

```
,------------------------------------------------------------,
| P1:$999999999  P2:$999999999  P3:$999999999  P4:$999999999 |
| SB: P1 $XXXX   LB: P2 $XXXX   Ante:$XXXX   Pot:$XXXXXXXXXX |
|                                                            |
|  P2:$999999999        P3:$999999999        P4:$99999999    |
|  ,----,  ,----,       ,----,  ,----,       ,----,  ,----,  |
|  |♠  8|  |♢  A|       |♡  8|  |♢  3|       |♣  J|  |♠  J|  |
|  |    |  |    |       |    |  |    |       |    |  |    |  |
|  |8  ♠|  |A  ♢|       |8  ♡|  |3  ♢|       |J  ♣|  |J  ♠|  |
|  '----'  '----'       '----'  '----'       '----'  '----'  |
|                                                            |
|  ,----,  ,----,  ,----,  ,----,  ,----,                    |
|  |♡ 10|  |♢  9|  |♢  J|  |♣  7|  |♡  J|                    |
|  |    |  |    |  |    |  |    |  |    |                    |
|  |10 ♡|  |9  ♢|  |J  ♢|  |7  ♣|  |J  ♡|                    |
|  '----'  '----'  '----'  '----'  '----'                    |
|                                                            |
|      _/         _/_/     _/_/_/  _/_/_/_/  ,----,  ,----,  |
|     _/       _/    _/ _/        _/         |♣  4|  |♣  K|  |
|    _/       _/    _/   _/_/    _/_/_/      |    |  |    |  |
|   _/       _/    _/       _/  _/           |4  ♣|  |K  ♣|  |
|  _/_/_/_/   _/_/   _/_/_/    _/_/_/_/      '----'  '----'  |
|                                                            |
|____________________________________________________________|
|,----------------------------------------------------------,|
|| Player Four wins!                                        ||
|| Four of a Kind                                           ||
||                                                          ||
||__________________________________________________________||
'------------------------------------------------------------'
```

# Current State  
When you run the game, cards get shuffled and dealt to P1, P2, P3, and P4. Pressing any key on your keyboard will progress the game through each stage of Texas Hold 'em (minus the betting bits). At the end, the hands are compared, a winner is displayed, and the application exits.

# Install  
(Note: Binaries will be published after initial AI logic)

## Prerequisites
- Install [Go 1.22 or higher](https://go.dev/doc/install)

## Build
- From the cloned repository root, run
```sh
# Linux, Mac, or Git-BASH
go build ./cmd/cli-texas-holdem.go
```

```powershell
# Powershell or Command Prompt
go build .\cmd\cli-texas-holdem.go
```

## Run  
```sh
# Linux or Mac
./cli-texas-holdem
```

```powershell
# Powershell, Command Prompt, or Git-BASH
.\cli-texas-holdem.exe
```