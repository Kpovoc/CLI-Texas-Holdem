package pkg

import (
	"fmt"
	"os"
	"os/exec"
	"runtime"
)

const cardTop = ",----,"
const cardMid = "|    |"
const cardBot = "'----'"
const cardBlank = "        "
const cardBack = "|.  .|"

const cardValTempl = "|%s  %s|"
const cardTenTempl = "|%s %s|"

func pickCardTempl(value int) string {
	if value == 10 {
		return cardTenTempl
	}

	return cardValTempl
}

func runCmd(name string, arg ...string) {
	cmd := exec.Command(name, arg...)
	cmd.Stdout = os.Stdout
	_ = cmd.Run()
}

func clearTerminal() {
	switch runtime.GOOS {
	case "darwin":
		runCmd("clear")
	case "linux":
		runCmd("clear")
	case "windows":
		runCmd("cmd", "/c", "cls")
	default:
		runCmd("clear")
	}
}

func repeatString(value string, count int) string {
	ret := ""
	for i := 0; i < count; i++ {
		ret += value
	}

	return ret
}

func cardRow(n int, str string, blanks int) string {
	ret := ""
	for _ = range n {
		ret += str + "  "
	}

	for i := 0; i < blanks; i++ {
		ret += cardBlank
	}

	return ret
}

func cardValueRow(values []string, blanks int) string {
	ret := ""
	for _, value := range values {
		ret += value + "  "
	}

	for i := 0; i < blanks; i++ {
		ret += cardBlank
	}

	return ret
}

func determineCardDisplayValues(cards []Card) (topValues, botValues []string) {
	for _, card := range cards {
		templ := pickCardTempl(card.GetValue())
		topValues = append(topValues, fmt.Sprintf(templ, card.SuitDisplay(), card.FaceDisplay()))
		botValues = append(botValues, fmt.Sprintf(templ, card.FaceDisplay(), card.SuitDisplay()))
	}

	return topValues, botValues
}

func drawStats() (rows []string) {
	return []string{
		",------------------------------------------------------------,",
		"| P1:$999999999  P2:$999999999  P3:$999999999  P4:$999999999 |",
		"| SB: P1 $XXXX   LB: P2 $XXXX   Ante:$XXXX   Pot:$XXXXXXXXXX |",
		"|                                                            |",
		"|  P2:$999999999        P3:$999999999        P4:$99999999    |",
	}
}

func drawOpponentsHands(rows []string, state int, players []Player) []string {
	if state == EMPTY {
		for i := 0; i < 5; i++ {
			rows = drawBlankRow(rows)
		}

		return rows
	}

	rowStart := "|  "
	pad := "     "
	rowEnd := "|"
	var topValues []string
	var botValues []string

	newRows := []string{rowStart, rowStart, rowStart, rowStart, rowStart}
	if state == SHOWDOWN {
		for _, player := range players {
			if player.folded {
				topValues = append(topValues, cardBack)
				botValues = append(botValues, cardBack)
			} else {
				tv, bv := determineCardDisplayValues(player.cards)
				topValues = append(topValues, tv...)
				botValues = append(botValues, bv...)
			}
		}
	} else {
		for _, _ = range players {
			topValues = append(topValues, cardBack, cardBack)
			botValues = append(botValues, cardBack, cardBack)
		}
	}

	newRows[0] += cardRow(2, cardTop, 0) +
		pad +
		cardRow(2, cardTop, 0) +
		pad +
		cardRow(2, cardTop, 0) +
		rowEnd
	newRows[1] += cardValueRow(topValues[0:2], 0) +
		pad +
		cardValueRow(topValues[2:4], 0) +
		pad +
		cardValueRow(topValues[4:6], 0) +
		rowEnd
	newRows[2] += cardRow(2, cardMid, 0) +
		pad +
		cardRow(2, cardMid, 0) +
		pad +
		cardRow(2, cardMid, 0) +
		rowEnd
	newRows[3] += cardValueRow(botValues[0:2], 0) +
		pad +
		cardValueRow(botValues[2:4], 0) +
		pad +
		cardValueRow(botValues[4:6], 0) +
		rowEnd
	newRows[4] += cardRow(2, cardBot, 0) +
		pad +
		cardRow(2, cardBot, 0) +
		pad +
		cardRow(2, cardBot, 0) +
		rowEnd

	return append(rows, newRows...)
}

func drawInPlay(rows []string, cards []Card) []string {
	rowStart := "|  "
	rowEnd := "                  |"
	nCards := len(cards)
	nMissing := 5 - nCards
	topValues, botValues := determineCardDisplayValues(cards)

	newRows := []string{rowStart, rowStart, rowStart, rowStart, rowStart}
	newRows[0] += cardRow(nCards, cardTop, nMissing) + rowEnd
	newRows[1] += cardValueRow(topValues, nMissing) + rowEnd
	newRows[2] += cardRow(nCards, cardMid, nMissing) + rowEnd
	newRows[3] += cardValueRow(botValues, nMissing) + rowEnd
	newRows[4] += cardRow(nCards, cardBot, nMissing) + rowEnd

	return append(rows, newRows...)
}

var wRows = []string{
	"   _/         _/  _/_/_/  _/      _/      ",
	"  _/         _/    _/    _/_/    _/       ",
	" _/   _/    _/    _/    _/  _/  _/        ",
	"  _/ _/  _/      _/    _/    _/_/         ",
	"   _/ _/      _/_/_/  _/      _/          ",
}

var lRows = []string{
	"    _/         _/_/     _/_/_/  _/_/_/_/  ",
	"   _/       _/    _/ _/        _/         ",
	"  _/       _/    _/   _/_/    _/_/_/      ",
	" _/       _/    _/       _/  _/           ",
	"_/_/_/_/   _/_/   _/_/_/    _/_/_/_/      ",
}

const eRow = "                                          "

func drawPlayerHand(rows []string, cards []Card, gameOver bool, didWin bool) []string {
	rowStart := "|  "
	rowEnd := "|"
	nCards := len(cards)
	nMissing := 2 - nCards
	topValues, botValues := determineCardDisplayValues(cards)

	newRows := []string{rowStart, rowStart, rowStart, rowStart, rowStart}
	if !gameOver {
		newRows[0] += eRow + cardRow(nCards, cardTop, nMissing) + rowEnd
		newRows[1] += eRow + cardValueRow(topValues, nMissing) + rowEnd
		newRows[2] += eRow + cardRow(nCards, cardMid, nMissing) + rowEnd
		newRows[3] += eRow + cardValueRow(botValues, nMissing) + rowEnd
		newRows[4] += eRow + cardRow(nCards, cardBot, nMissing) + rowEnd

		return append(rows, newRows...)
	}

	if didWin {
		newRows[0] += wRows[0] + cardRow(nCards, cardTop, nMissing) + rowEnd
		newRows[1] += wRows[1] + cardValueRow(topValues, nMissing) + rowEnd
		newRows[2] += wRows[2] + cardRow(nCards, cardMid, nMissing) + rowEnd
		newRows[3] += wRows[3] + cardValueRow(botValues, nMissing) + rowEnd
		newRows[4] += wRows[4] + cardRow(nCards, cardBot, nMissing) + rowEnd

		return append(rows, newRows...)
	}

	newRows[0] += lRows[0] + cardRow(nCards, cardTop, nMissing) + rowEnd
	newRows[1] += lRows[1] + cardValueRow(topValues, nMissing) + rowEnd
	newRows[2] += lRows[2] + cardRow(nCards, cardMid, nMissing) + rowEnd
	newRows[3] += lRows[3] + cardValueRow(botValues, nMissing) + rowEnd
	newRows[4] += lRows[4] + cardRow(nCards, cardBot, nMissing) + rowEnd

	return append(rows, newRows...)
}

func drawTextBox(rows []string, game Game) []string {
	emptyRow := "||                                                          ||"
	textRow1 := ""
	textRow2 := emptyRow

	switch game.GetGameState() {
	case EMPTY:
		textRow1 = emptyRow
		break
	case PREFLOP:
		textRow1 = "|| PREFLOP                                                  ||"
		break
	case FLOP:
		textRow1 = "|| FLOP                                                     ||"
		break
	case TURN:
		textRow1 = "|| TURN                                                     ||"
		break
	case RIVER:
		textRow1 = "|| RIVER                                                    ||"
		break
	case SHOWDOWN:
		textRow1 = "|| " + game.winner + " wins!"
		textRow1 += repeatString(" ", 60-len(textRow1)) + "||"
		textRow2 = "|| " + game.winningHand.Name
		textRow2 += repeatString(" ", 60-len(textRow2)) + "||"
		break
	case EXIT:
		textRow1 = "|| EXIT                                                     ||"
		break
	}

	newRows := []string{
		"|____________________________________________________________|",
		"|,----------------------------------------------------------,|",
	}
	newRows = append(
		newRows,
		textRow1,
		textRow2,
		emptyRow,
		"||__________________________________________________________||",
		"'------------------------------------------------------------'",
	)

	return append(rows, newRows...)
}

func drawBlankRow(rows []string) []string {
	return append(rows, "|                                                            |")
}

func DrawBoard(game Game) {
	clearTerminal()

	player := game.players[0]
	isGameOver := game.GetGameState() == SHOWDOWN
	didPlayerWin := game.winner == player.name

	rows := drawStats()
	rows = drawOpponentsHands(
		rows,
		game.GetGameState(),
		game.players[1:4])
	rows = drawBlankRow(rows)
	rows = drawInPlay(rows, game.GetBoard())
	rows = drawBlankRow(rows)
	rows = drawPlayerHand(rows, player.cards, isGameOver, didPlayerWin)
	rows = drawBlankRow(rows)
	rows = drawTextBox(rows, game)

	for _, row := range rows {
		fmt.Println(row)
	}
}

func RenderCards(cards []Card) {
	totalCards := len(cards)
	topRow := cardRow(totalCards, cardTop, 0)
	midRow := cardRow(totalCards, cardMid, 0)
	botRow := cardRow(totalCards, cardBot, 0)
	topVals := ""
	botVals := ""

	for _, card := range cards {
		templ := pickCardTempl(card.GetValue())
		topVals += fmt.Sprintf(templ, card.SuitDisplay(), card.FaceDisplay()) + "  "
		botVals += fmt.Sprintf(templ, card.FaceDisplay(), card.SuitDisplay()) + "  "
	}

	fmt.Println(topRow)
	fmt.Println(topVals)
	fmt.Println(midRow)
	fmt.Println(botVals)
	fmt.Println(botRow)
}

/*
,------------------------------------------------------------,
| P1:$999999999  P2:$999999999  P3:$999999999  P4:$999999999 |
| SB: P1 $XXXX   LB: P2 $XXXX   Ante:$XXXX   Pot:$XXXXXXXXXX |
|                                                            |
|  P2:$999999999        P3:$999999999        P4:$99999999    |
|  ,----,  ,----,       ,----,  ,----,       ,----,  ,----,  |
|  |    |  |    |       |    |  |    |       |    |  |    |  |
|  |    |  |    |       |    |  |    |       |    |  |    |  |
|  |    |  |    |       |    |  |    |       |    |  |    |  |
|  '----'  '----'       '----'  '----'       '----'  '----'  |
|                                                            |
|  ,----,  ,----,  ,----,  ,----,  ,----,                    |
|  |♤  7|  |♧  8|  |♥  9|  |♦ 10|  |♤  J|                    |
|  |    |  |    |  |    |  |    |  |    |                    |
|  |7  ♤|  |8  ♧|  |9  ♥|  |10 ♦|  |J  ♤|                    |
|  '----'  '----'  '----'  '----'  '----'                    |
|                                                            |
|     _/         _/  _/_/_/  _/      _/      ,----,  ,----,  |
|    _/         _/    _/    _/_/    _/       |♧  Q|  |♥  K|  |
|   _/   _/    _/    _/    _/  _/  _/        |    |  |    |  |
|    _/ _/  _/      _/    _/    _/_/         |Q  ♧|  |K  ♥|  |
|     _/ _/      _/_/_/  _/      _/          '----'  '----'  |
|                                                            |
|____________________________________________________________|
|,----------------------------------------------------------,|
||                                                          ||
||                                                          ||
||                                                          ||
||__________________________________________________________||
'------------------------------------------------------------'

,------------------------------------------------------------,
|                                                            |
|                                                            |
|                                                            |
|  ,----,  ,----,  ,----,  ,----,  ,----,    ,----,  ,----,  |
|  |    |  |    |  |    |  |    |  |    |    |    |  |    |  |
|  |    |  |    |  |    |  |    |  |    |    |    |  |    |  |
|  |    |  |    |  |    |  |    |  |    |    |    |  |    |  |
|  '----'  '----'  '----'  '----'  '----'    '----'  '----'  |
|  ,----,  ,----,  ,----,  ,----,  ,----,                    |
|  |    |  |    |  |    |  |    |  |    |                    |
|  |    |  |    |  |    |  |    |  |    |                    |
|  |    |  |    |  |    |  |    |  |    |                    |
|  '----'  '----'  '----'  '----'  '----'                    |
|                                                            |
|      _/         _/_/     _/_/_/  _/_/_/_/  ,----,  ,----,  |
|     _/       _/    _/ _/        _/         |    |  |    |  |
|    _/       _/    _/   _/_/    _/_/_/      |    |  |    |  |
|   _/       _/    _/       _/  _/           |    |  |    |  |
|  _/_/_/_/   _/_/   _/_/_/    _/_/_/_/      '----'  '----'  |
|                                                            |
|____________________________________________________________|
|,----------------------------------------------------------,|
||                                                          ||
||                                                          ||
||                                                          ||
||__________________________________________________________||
'------------------------------------------------------------'


*/
