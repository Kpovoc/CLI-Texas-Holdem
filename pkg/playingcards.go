package pkg

import "math/rand"

type Suit struct {
	Name   string
	Symbol string
}

var (
	SPADES   Suit = Suit{"Spades", "♠"}
	HEARTS   Suit = Suit{"Hearts", "♡"}
	CLUBS    Suit = Suit{"Clubs", "♣"}
	DIAMONDS Suit = Suit{"Diamonds", "♢"}
)

type Face struct {
	Name   string
	Symbol string
	Value  int
	Hex    byte
}

var (
	TWO   Face = Face{"Two", "2", 0x2, '2'}
	THREE Face = Face{"Three", "3", 0x3, '3'}
	FOUR  Face = Face{"Four", "4", 0x4, '4'}
	FIVE  Face = Face{"Five", "5", 0x5, '5'}
	SIX   Face = Face{"Six", "6", 0x6, '6'}
	SEVEN Face = Face{"Seven", "7", 0x7, '7'}
	EIGHT Face = Face{"Eight", "8", 0x8, '8'}
	NINE  Face = Face{"Nine", "9", 0x9, '9'}
	TEN   Face = Face{"Ten", "10", 0xA, 'A'}
	JACK  Face = Face{"Jack", "J", 0xB, 'B'}
	QUEEN Face = Face{"Queen", "Q", 0xC, 'C'}
	KING  Face = Face{"King", "K", 0xD, 'D'}
	ACE   Face = Face{"Ace", "A", 0xE, 'E'}
)

var (
	SUITS = []Suit{SPADES, HEARTS, CLUBS, DIAMONDS}
	FACES = []Face{TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE}
)

type Card struct {
	Face Face
	Suit Suit
}

func (c Card) GetName() string {
	return c.Face.Name + " of " + c.Suit.Name
}

func (c Card) SuitDisplay() string {
	return c.Suit.Symbol
}

func (c Card) FaceDisplay() string {
	return c.Face.Symbol
}

func (c Card) GetValue() int {
	return c.Face.Value
}

func (c Card) GetHexChar() byte {
	return c.Face.Hex
}

type Deck struct {
	Cards []Card
}

func NewDeck() Deck {
	var cards []Card

	for _, suit := range SUITS {
		for _, face := range FACES {
			cards = append(cards, Card{Face: face, Suit: suit})
		}
	}

	return Deck{Cards: cards}
}

func (d *Deck) IsEmpty() bool {
	return len(d.Cards) == 0
}

func (d *Deck) DrawCard() Card {
	card := d.Cards[0]
	d.Cards = d.Cards[1:]
	return card
}

func (d *Deck) Shuffle() {
	for i := range d.Cards {
		j := rand.Intn(i + 1)
		d.Cards[i], d.Cards[j] = d.Cards[j], d.Cards[i]
	}
}
