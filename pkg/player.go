package pkg

type Player struct {
	cards  []Card
	folded bool
	name   string
	money  int
}

func NewPlayer(name string, money int) Player {
	return Player{name: name, money: money}
}

func (p *Player) ReceiveDealtCards(card Card, card2 Card) {
	p.cards = append(p.cards, card, card2)
}

func (p *Player) AddToMoney(amount int) {
	p.money += amount
}

func (p *Player) RemoveFromMoney(amount int) {
	p.money -= amount
}
