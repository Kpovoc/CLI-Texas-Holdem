package pkg

import (
	"cmp"
	"slices"
	"strconv"
)

const (
	HIGH_CARD       = '0'
	ONE_PAIR        = '1'
	TWO_PAIRS       = '2'
	THREE_OF_A_KIND = '3'
	STRAIGHT        = '4'
	FLUSH           = '5'
	FULL_HOUSE      = '6'
	FOUR_OF_A_KIND  = '7'
	STRAIGHT_FLUSH  = '8'
)

type Hand struct {
	CardIndices []int
	Value       int64
	Name        string
	PlayerName  string
}
type SelectedCard struct {
	Card  Card
	Index int
}

func cardCompare(card1, card2 Card) int {
	return cmp.Compare(card1.GetValue(), card2.GetValue()) * -1
}

func selectedCardCompare(card1, card2 SelectedCard) int {
	return cardCompare(card1.Card, card2.Card)
}

func groupCardsBySuit(cards []SelectedCard) (hearts []SelectedCard, diamonds []SelectedCard, spades []SelectedCard, clubs []SelectedCard) {
	for _, card := range cards {
		switch card.Card.SuitDisplay() {
		case HEARTS.Symbol:
			hearts = append(hearts, card)
			break
		case DIAMONDS.Symbol:
			diamonds = append(diamonds, card)
			break
		case SPADES.Symbol:
			spades = append(spades, card)
			break
		case CLUBS.Symbol:
			clubs = append(clubs, card)
			break
		default:
		}
	}

	return hearts, diamonds, spades, clubs
}

func groupCardsByFace(cards []SelectedCard) (faceGroup [][]SelectedCard) {
	previousValue := 0
	groupIndex := -1
	for _, card := range cards {
		if card.Card.GetValue() == previousValue {
			faceGroup[groupIndex] = append(faceGroup[groupIndex], card)
		} else {
			groupIndex++
			previousValue = card.Card.GetValue()
			faceGroup = append(faceGroup, []SelectedCard{card})
		}
	}

	return faceGroup
}

func pickFlushGroup(hearts []SelectedCard, diamonds []SelectedCard, spades []SelectedCard, clubs []SelectedCard) []SelectedCard {
	if len(hearts) >= 5 {
		return hearts
	} else if len(diamonds) >= 5 {
		return diamonds
	} else if len(spades) >= 5 {
		return spades
	} else if len(clubs) >= 5 {
		return clubs
	} else {
		return []SelectedCard{}
	}
}

func addRemainingCardsToValue(cards []SelectedCard, selectedIndices []int, hexBytes []byte) []byte {
	for _, card := range cards {
		if !slices.Contains(selectedIndices, card.Index) {
			hexBytes = append(hexBytes, card.Card.GetHexChar())
		}
	}

	return hexBytes
}

func selectedIndicesForFlush(cards []SelectedCard, hexBytes []byte) ([]int, []byte) {
	var selectedIndices []int
	for i, card := range cards {
		if i > 4 {
			break
		}
		selectedIndices = append(selectedIndices, card.Index)
		hexBytes = append(hexBytes, card.Card.GetHexChar())
	}

	return selectedIndices, hexBytes
}

func selectedIndicesForStraight(cards []SelectedCard, givenHexBytes []byte) ([]int, []byte) {
	var selectedIndices []int
	cardLength := len(cards)
	hexBytes := givenHexBytes

	for startingIndex := 0; cardLength-startingIndex >= 5; startingIndex++ {
		remainingCards := cards[startingIndex:]

		for index, card := range remainingCards {
			if len(selectedIndices) == 4 || index == len(remainingCards)-1 {
				selectedIndices = append(selectedIndices, card.Index)
				hexBytes = append(hexBytes, card.Card.GetHexChar())
				break
			}

			next := remainingCards[index+1]
			diff := card.Card.GetValue() - next.Card.GetValue()

			if diff == 0 {
				continue
			} else if diff == 1 {
				selectedIndices = append(selectedIndices, card.Index)
				hexBytes = append(hexBytes, card.Card.GetHexChar())
			} else {
				selectedIndices = []int{}
				hexBytes = givenHexBytes
				break
			}
		}

		if len(selectedIndices) == 5 {
			return selectedIndices, hexBytes
		} else {
			selectedIndices = []int{}
			hexBytes = givenHexBytes
		}
	}
	return []int{}, givenHexBytes
}

func isStraightFlush(cards []SelectedCard) (bool, Hand) {
	ret := Hand{}
	hearts, diamonds, spades, clubs := groupCardsBySuit(cards)

	flushGroup := pickFlushGroup(hearts, diamonds, spades, clubs)
	if len(flushGroup) == 0 {
		return false, ret
	}

	selectedIndices, hexBytes := selectedIndicesForStraight(flushGroup, []byte{STRAIGHT_FLUSH})
	if len(selectedIndices) == 0 {
		return false, ret
	}

	hexBytes = addRemainingCardsToValue(cards, selectedIndices, hexBytes)

	ret.CardIndices = selectedIndices
	ret.Value, _ = strconv.ParseInt(string(hexBytes), 16, 64)
	if hexBytes[1] == ACE.Hex &&
		hexBytes[2] == KING.Hex &&
		hexBytes[3] == QUEEN.Hex &&
		hexBytes[4] == JACK.Hex &&
		hexBytes[5] == TEN.Hex {
		ret.Name = "Royal Flush"
	} else {
		ret.Name = "Straight Flush"
	}

	return true, ret
}

func isFourOfAKind(cards []SelectedCard) (bool, Hand) {
	ret := Hand{}
	facesGroup := groupCardsByFace(cards)

	for _, group := range facesGroup {
		if len(group) >= 4 {
			var selectedIndices []int
			hexBytes := []byte{FOUR_OF_A_KIND}

			for _, card := range group {
				selectedIndices = append(selectedIndices, card.Index)
				hexBytes = append(hexBytes, card.Card.GetHexChar())
			}

			hexBytes = addRemainingCardsToValue(cards, selectedIndices, hexBytes)

			ret.CardIndices = selectedIndices
			ret.Value, _ = strconv.ParseInt(string(hexBytes), 16, 64)
			ret.Name = "Four of a Kind"
			return true, ret
		}
	}
	return false, ret
}

func isFullHouse(cards []SelectedCard) (bool, Hand) {
	ret := Hand{}
	facesGroup := groupCardsByFace(cards)

	var twoGroup []SelectedCard
	var threeGroup []SelectedCard
	for _, group := range facesGroup {
		if len(group) == 3 {
			threeGroup = group
		} else if len(group) == 2 && len(twoGroup) == 0 {
			twoGroup = group
		}
	}

	if len(twoGroup) == 0 || len(threeGroup) == 0 {
		return false, ret
	}

	var selectedIndices []int
	hexBytes := []byte{FULL_HOUSE}
	for _, card := range threeGroup {
		selectedIndices = append(selectedIndices, card.Index)
		hexBytes = append(hexBytes, card.Card.GetHexChar())
	}
	for _, card := range twoGroup {
		selectedIndices = append(selectedIndices, card.Index)
		hexBytes = append(hexBytes, card.Card.GetHexChar())
	}

	hexBytes = addRemainingCardsToValue(cards, selectedIndices, hexBytes)

	ret.CardIndices = selectedIndices
	ret.Value, _ = strconv.ParseInt(string(hexBytes), 16, 64)
	ret.Name = "Full House"
	return true, ret
}

func isThreeOfAKind(cards []SelectedCard) (bool, Hand) {
	ret := Hand{}
	facesGroup := groupCardsByFace(cards)

	for _, group := range facesGroup {
		if len(group) >= 3 {
			var selectedIndices []int
			hexBytes := []byte{THREE_OF_A_KIND}

			for _, card := range group {
				selectedIndices = append(selectedIndices, card.Index)
				hexBytes = append(hexBytes, card.Card.GetHexChar())
			}

			hexBytes = addRemainingCardsToValue(cards, selectedIndices, hexBytes)

			ret.CardIndices = selectedIndices
			ret.Value, _ = strconv.ParseInt(string(hexBytes), 16, 64)
			ret.Name = "Three of a Kind"
			return true, ret
		}
	}
	return false, ret
}

func isFlush(cards []SelectedCard) (bool, Hand) {
	ret := Hand{}
	hearts, diamonds, spades, clubs := groupCardsBySuit(cards)

	flushGroup := pickFlushGroup(hearts, diamonds, spades, clubs)
	if len(flushGroup) == 0 {
		return false, ret
	}

	selectedIndices, hexBytes := selectedIndicesForFlush(flushGroup, []byte{FLUSH})
	hexBytes = addRemainingCardsToValue(cards, selectedIndices, hexBytes)

	ret.CardIndices = selectedIndices
	ret.Value, _ = strconv.ParseInt(string(hexBytes), 16, 64)
	ret.Name = "Flush"

	return true, ret
}

func isStraight(cards []SelectedCard) (bool, Hand) {
	ret := Hand{}

	selectedIndices, hexBytes := selectedIndicesForStraight(cards, []byte{STRAIGHT})
	if len(selectedIndices) == 0 {
		return false, ret
	}

	hexBytes = addRemainingCardsToValue(cards, selectedIndices, hexBytes)

	ret.CardIndices = selectedIndices
	ret.Value, _ = strconv.ParseInt(string(hexBytes), 16, 64)
	ret.Name = "Straight"

	return true, ret
}

func isTwoPairs(cards []SelectedCard) (bool, Hand) {
	ret := Hand{}
	facesGroup := groupCardsByFace(cards)

	var selectedIndices []int
	hexBytes := []byte{TWO_PAIRS}

	for _, group := range facesGroup {
		if len(group) >= 2 {
			for _, card := range group {
				selectedIndices = append(selectedIndices, card.Index)
				hexBytes = append(hexBytes, card.Card.GetHexChar())
			}

			if len(selectedIndices) == 4 {
				break
			}
		}
	}

	if len(selectedIndices) != 4 {
		return false, ret
	}

	hexBytes = addRemainingCardsToValue(cards, selectedIndices, hexBytes)

	ret.CardIndices = selectedIndices
	ret.Value, _ = strconv.ParseInt(string(hexBytes), 16, 64)
	ret.Name = "Two Pairs"
	return true, ret
}

func isOnePair(cards []SelectedCard) (bool, Hand) {
	ret := Hand{}
	facesGroup := groupCardsByFace(cards)

	for _, group := range facesGroup {
		if len(group) >= 2 {
			var selectedIndices []int
			hexBytes := []byte{ONE_PAIR}

			for _, card := range group {
				selectedIndices = append(selectedIndices, card.Index)
				hexBytes = append(hexBytes, card.Card.GetHexChar())
			}

			hexBytes = addRemainingCardsToValue(cards, selectedIndices, hexBytes)

			ret.CardIndices = selectedIndices
			ret.Value, _ = strconv.ParseInt(string(hexBytes), 16, 64)
			ret.Name = "One Pair"
			return true, ret
		}
	}
	return false, ret
}

func highCardHand(cards []SelectedCard) Hand {
	indices := []int{cards[0].Index}

	hexBytes := []byte{HIGH_CARD}
	for _, card := range cards {
		hexBytes = append(hexBytes, card.Card.GetHexChar())
	}

	value, _ := strconv.ParseInt(string(hexBytes), 16, 64)

	return Hand{
		CardIndices: indices,
		Value:       value,
		Name:        "High Card " + cards[0].Card.GetName(),
	}
}

func DetermineHand(cards []Card, playerName string) Hand {
	selectedCards := make([]SelectedCard, len(cards))
	for index, card := range cards {
		selectedCards[index] = SelectedCard{card, index}
	}

	slices.SortFunc(selectedCards, selectedCardCompare)

	var playerHand Hand
	if isStrF, hand := isStraightFlush(selectedCards); isStrF {
		playerHand = hand
	} else if is4OAK, hand := isFourOfAKind(selectedCards); is4OAK {
		playerHand = hand
	} else if isFH, hand := isFullHouse(selectedCards); isFH {
		playerHand = hand
	} else if isFl, hand := isFlush(selectedCards); isFl {
		playerHand = hand
	} else if isStr, hand := isStraight(selectedCards); isStr {
		playerHand = hand
	} else if is3OAK, hand := isThreeOfAKind(selectedCards); is3OAK {
		playerHand = hand
	} else if isTP, hand := isTwoPairs(selectedCards); isTP {
		playerHand = hand
	} else if isOP, hand := isOnePair(selectedCards); isOP {
		playerHand = hand
	} else {
		playerHand = highCardHand(selectedCards)
	}

	playerHand.PlayerName = playerName
	return playerHand
}

/**
,----, ,----, ,----, ,----, ,----,
|♡  3| |♠  3| |♠  9| |♠  K| |♡  9|
|    | |    | |    | |    | |    |
|3  ♡| |3  ♠| |9  ♠| |K  ♠| |9  ♡|
'----' '----' '----' '----' '----'
Hand A: Two Pairs
,----, ,----,
|♣  2| |♠  4|
|    | |    |
|2  ♣| |4  ♠|
'----' '----'
Hand B: One Pair
,----, ,----,
|♡  7| |♢  7|
|    | |    |
|7  ♡| |7  ♢|
'----' '----'
*/
