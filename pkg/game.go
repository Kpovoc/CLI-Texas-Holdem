package pkg

const (
	EMPTY = iota
	PREFLOP
	FLOP
	TURN
	RIVER
	SHOWDOWN
	EXIT
)

type Game struct {
	deck        Deck
	players     []Player
	winner      string
	board       []Card
	winningHand Hand
	state       int
}

func (game *Game) Init() {
	game.deck = NewDeck()
	game.deck.Shuffle()

	game.players = []Player{
		NewPlayer("Player One", 10000),
		NewPlayer("Player Two", 10000),
		NewPlayer("Player Three", 10000),
		NewPlayer("Player Four", 10000),
	}
	game.state = EMPTY
}

func (game *Game) GetGameState() int {
	return game.state
}

func (game *Game) GetBoard() []Card {
	return game.board
}

func (game *Game) GetWinningHand() Hand {
	return game.winningHand
}

func (game *Game) GetPlayerCards(playerNum int) []Card {
	return game.players[playerNum-1].cards
}

func (game *Game) GetPlayerMoney(playerNum int) int {
	return game.players[playerNum-1].money
}

func (game *Game) AdvanceGame() {
	switch game.state {
	case EMPTY:
		game.state = PREFLOP
		game.runPreFlop()
		break
	case PREFLOP:
		game.state = FLOP
		game.runFlop()
		break
	case FLOP:
		game.state = TURN
		game.runTurnOrRiver()
		break
	case TURN:
		game.state = RIVER
		game.runTurnOrRiver()
		break
	case RIVER:
		game.state = SHOWDOWN
		game.runShowDown()
		break
	case SHOWDOWN:
		game.state = EMPTY
		game.runNextRound()
		break
	}
}

/* Game State Functions */
func (game *Game) runPreFlop() {
	game.dealCards()
}

func (game *Game) runFlop() {
	game.dealTheFlop()
}

func (game *Game) runTurnOrRiver() {
	game.burnAndDealToBoard()
}

func (game *Game) runShowDown() {
	game.determineWinner()
}

func (game *Game) runNextRound() {
	for i := 0; i < len(game.players); i++ {
		game.players[i].folded = false
		game.players[i].cards = []Card{}
	}
	game.deck = NewDeck()
	game.deck.Shuffle()
}

/* Help Functions */
func (game *Game) dealCards() {
	for i := 0; i < len(game.players); i++ {
		game.players[i].cards = []Card{game.deck.DrawCard(), game.deck.DrawCard()}
	}
}
func (game *Game) dealTheFlop() {
	_ = game.deck.DrawCard()
	game.board = []Card{
		game.deck.DrawCard(),
		game.deck.DrawCard(),
		game.deck.DrawCard(),
	}
}
func (game *Game) burnAndDealToBoard() {
	_ = game.deck.DrawCard()
	game.board = append(game.board, game.deck.DrawCard())
}
func (game *Game) determineWinner() {
	var highestHand Hand
	for _, player := range game.players {
		hand := DetermineHand(append(game.GetBoard(), player.cards...), player.name)
		if highestHand.Value < hand.Value {
			highestHand = hand
		}
	}

	game.winner = highestHand.PlayerName
	game.winningHand = highestHand
}
